# THOT

Proyecto opensource de maquina braille de bajo costo.

## ....

La maquina esta diseñada para utilizarse en medios de pocos recursos, con unos perdiles estandar de 2020 de aluminio extruido.
Nombre vasado en el dios egiptcio THOT (https://es.wikipedia.org/wiki/Thot_(dios_egipcio)) 

## UN POCO DE HISTORIA

Dada la escased de maquinas braille, y su gran costo, cerca de 4.000€, nos hemos visto obligados desde la ong MH MAKER HELP a diseñar, fabricar, y
liberar el diseño de un modelo de bajo cosoto y facil construcion, siendo este no superior a unos 100/150€.

Diseñada y montada en menos de 72 horas para presentarse en la convencion maker de cometcon gijon. esta hecha en materiales reciclados y diseñada para papel de 120gr y de un tamaño superior a din A3, siendo posible su calibracion para papeles: Dian A4 Din A5 Din A6 o superiores.

Diseñada con perfiles 2020 Gslot de la empresa gallega Makergal. https://www.makergal.es/product-page/perfil-g-slot-2020-anodizado-negro

## CONTROL

Utilizamos el marlin clasico para una cnc o laser. (subiremos en un tiempo el marlin configurado con parametros estandar.)

## Gcode

Para la generacion del codigo G usamos la web ya disponbible francesa https://crocsg.github.io/BrailleRap/


## FUTURAS MEJORAS

Encapsulamiento del producto parar poder operar en entornos con menores.
Reducion del gasto de material plastico a la hora de fabricacion.

## CONTRIBUCIONES

Makegal https://www.makergal.es/
smartmaterials https://www.smartmaterials3d.com/
abadia tecnologica https://abadiatecnologica.es/
FAE burgos https://www.faeburgos.org/
Gams3d https://www.gams3d.com/
Staticboards https://www.staticboards.com/

## AUTOR Y DISEÑADORES

ONG MH MAKER HELP. En especial el diseñador https://www.instagram.com/el_elfo_3d/ 

## LICENCIA

Creative Commons Atribución/Reconocimiento-CompartirIgual 4.0 Licencia Pública Internacional
— CC BY-SA 4.0

## ESTATUS DEL Proyecto

En trabajo de mejora!!

